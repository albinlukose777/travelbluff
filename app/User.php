<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use function PHPSTORM_META\override;

class User extends Authenticatable
{
    use Notifiable,Followable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','username','avatar',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

        public function tweets()
    {
        return $this->hasMany(Tweet::class);; 
    }
    public function timeline()
    {
        $friendsid=$this->follows()->pluck('id');
        return Tweet::whereIn('user_id', $friendsid)
        ->orWhere('user_id',$this->id)
        ->withLikes()
        ->latest()->paginate(10); 
    }
    public function getAvatarAttribute($value)
    {
        $disk = Storage::disk('s3');
        $url =$value?($disk->getAwsTemporaryUrl($disk->getDriver()->getAdapter(), $value, Carbon::now()->addMinutes(5), [])):'/images/default-profile-avatar.png';
        return asset($url);
        //return asset($value?'/storage'.$value:'/images/default-profile-avatar.jfif');
    }

    public function path($append = '')
    {
    $path = route('profile', $this->username);
return $append ? "{$path}/{$append}" : $path;
    }    
    public function likes()
    {
        return $this->hasMany(Like::class);
    }

    /*public function getRouteKeyName()
{
    return 'name';
}not needed above laravel 7*/
}
