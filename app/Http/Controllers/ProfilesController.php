<?php

namespace App\Http\Controllers;

use App\Tweet;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\User;
use Illuminate\Support\Facades\Hash;

class ProfilesController extends Controller
{
    public function show (User $user)
    {
        return view('profiles.show',[
            'user'=>$user,
            'tweets'=>$user->tweets()->withLikes()->paginate(4),
        ]);

    }
    public function edit (User $user)
    {
        //$this->authorize('edit',$user);
        return view('profiles.edit',compact('user'));

    }
    public function update (User $user)
    {
        if(is_null(request('password')))
        {
            $attributes=request()->validate([
                'name' => ['required', 'string', 'max:255'],
                'username' => ['required', 'string','alpha_dash', 'max:255',Rule::unique('users')->ignore($user)],
                'avatar'=>['file'],
                'email' => ['required', 'string', 'email', 'max:255',Rule::unique('users')->ignore($user)],                
                ]);
        }
        else{
            $attributes=request()->validate([
                'name' => ['required', 'string', 'max:255'],
                'username' => ['required', 'string','alpha_dash', 'max:255',Rule::unique('users')->ignore($user)],
                'avatar'=>['file'],
                'email' => ['required', 'string', 'email', 'max:255',Rule::unique('users')->ignore($user)],
                'password' => ['required', 'string', 'min:8', 'confirmed'],               
                ]);
                $attributes['password']= Hash::make($attributes['password']);
        }
        //$this->authorize('edit',$user);
        if(request('avatar'))
            $attributes['avatar']=request('avatar')->store('avatars','s3');
            $user->update($attributes);
            return redirect() -> route('profile',$user);
    }
}
