<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tweet;

class TweetLikesController extends Controller
{
    public function store(Tweet $tweet)
    {
    $tweet->like();
    //return
    return redirect()->back(); 
        
    }
    public function destroy(Tweet $tweet)
    {
        $tweet->dislike();
        return redirect()->back();
    }
}
