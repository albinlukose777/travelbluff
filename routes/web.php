<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::group(['middleware' => ['auth']], function () {
Route::post('/tweets', 'TweetController@store');
Route::get('/tweets', 'TweetController@index')->name('home');

Route::post('/tweets/{tweet}/like', 'TweetLikesController@store');

Route::delete('/tweets/{tweet}/like', 'TweetLikesController@destroy');

Route::get('/profiles/{user:username}', 'ProfilesController@show')
->name('profile');
Route::get('/profiles/{user:username}/edit', 'ProfilesController@edit')
->name('profile.edit')
->middleware('can:edit,user');

Route::patch('/profiles/{user:username}', 'ProfilesController@update')
->middleware('can:edit,user');

Route::post('/profiles/{user:username}/follow', 'FollowsController@store');

Route::get('/explore', 'ExploreController');

});

