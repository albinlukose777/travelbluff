<div class="border border-blue-400 rounded-lg px-8 py-6 mb-8">
   <form action="/tweets" method="POST">
      @csrf
      <textarea
         name="body"
         class="w-full outline-none"
         placeholder="what's up guys"
         required
        
         ></textarea>
         <hr class="my-4">
      <footer class="flex justify-between items-center">
         <img 
            src="{{auth()->user()->avatar}}"
            alt="your avatar"
            class="rounded-full mr-2" 
            width="40px"
            height="40px"
            >
         <button 
            type="submit" 
            class="bg-blue-400 rounded-lg shadow py-2 px-6 text-white text-sm hover:bg-blue-600 h-10"
            >Post It!!</button>
      </footer>
   </form>
   @error('body')
   <p class="text-red-500 text-sm">{{$message}}</p>
   @enderror
</div>