<x-master>
  <section class="px-8">
      <main class="container mx-auto">
          <div class="lg:flex lg:justify-between">
              <div class="lg:w-2/4 lg:flex flex-col">
                  <h1
                      class="mt-6 text-2xl font-bold text-gray-900 leading-tight sm:mt-8 sm:text-4xl lg:text-3xl xl:text-4xl">
                      Social Network Platform for
                      <br class="hidden lg:inline"><span class="text-indigo-500">hodophiles</span>
                  </h1>
                  <p class="mt-2 text-gray-600 sm:mt-4 sm:text-xl px-2 py-2">
                      This platform helps you to share your travel experiences and find people who loves travelling.
                  </p>
                  <div class="mt-4 sm:mt-6 lg:flex lg:justify-items-auto">
                      <a href="{{ route('register') }}"
                          class="w-1/3 text-center  inline-block m-2 px-5 py-3 rounded-lg shadow-lg bg-indigo-500 text-sm text-white uppercase tracking-wider font-semibold sm:text-base">Register
                      </a>

                      <a href="{{ route('login') }}"
                      class="w-1/3  text-center inline-block m-2 px-5 py-3 rounded-lg shadow-lg bg-indigo-500 text-sm text-white uppercase tracking-wider font-semibold sm:text-base">Login
                  </a>
                  </div>
                    

              </div>
              <div class="lg:w-2/4 mb-2">
                  <img class="rounded-md shadow-lg  object-cover object-center" src="/images/traveldiary.jpg"
                      alt="tarvel diary">
              </div>

          </div>
          <footer>                <div class="container mx-auto  text-secondary-500 text-center border-t border-gray-300">
              <p> Website developed by <a href="https://www.facebook.com/albinlukose777" class="text-secondary-900 font-bold underline">Albin Lukose</a></p>
            </div></footer>

      </main>
  </section>
</x-master>