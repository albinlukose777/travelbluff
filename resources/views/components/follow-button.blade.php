<form method="POST" action="{{route('profile',$user)}}/follow">
    @csrf

    
    <button 
    type="submit" 
    class="rounded-full shadow bg-blue-500 text-black text-white text-xs py-2 px-4"
    >{{current_user()->following($user)?'Unfollow':'Follow'}}</button>

</form>