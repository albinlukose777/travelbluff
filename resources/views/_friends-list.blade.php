<div class="bg-gray-200 border border-gray-300 rounded-lg py-4 px-6">
<h3 class="font-bold text-xl mb-2">Following</h3>
<ul>
	@forelse(current_user()->follows as $user)
   <li class="{{$loop->last?'':'mb-4'}}">
      <div>
         <a href="{{route('profile',$user)}}" class="flex items-center text-sm">
         <img 
            src="{{$user->avatar}}"
            alt=""
            class="rounded-full mr-2" 
            width="40px"
            height="40px"
            >
            {{$user->name}}	
         </a>
      </div>
   </li>
   @empty
   <p class="p-4 font-semibold text-sm">Click on explore to follow  someone!</p>
    @endforelse
</ul>
</div>