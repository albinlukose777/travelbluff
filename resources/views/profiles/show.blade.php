<x-app>

    <header class="mb-2 relative">
        <div class="relative">
            <img src="/images/default-profile-banner.jpg" alt="">
            <img src="{{$user->avatar}}" alt=""
                class="rounded-full mr-2 absolute bottom-0 transform -translate-x-1/2 translate-y-1/2" style="left:50%"
                width="150px">
        </div>

        <div class="flex justify-between items-center mb-6">
            <div style="max-width: 250px">
                <h2 class="font-bold text-2xl mb-0">{{$user->name}}</h2>
                <p class="text-sm">joined {{$user->created_at->diffForHumans()}}</p>

            </div>
            <div class="flex justify-between items-center">
                @can('edit', $user)
                <a href="{{route('profile.edit',$user)}}"
                    class="rounded-full border border-gray-300 text-black text-xs py-2 mr-2 px-4">Edit Profile</a>
                @endcan

                @unless(current_user()->is($user))
                <x-follow-button :user="$user"></x-follow-button>
                @endunless
            </div>
        </div>
        <p class="text-sm">About User These were very numerous, for the place was thickly inhabited, and a large group of
            the queer people clustered near, gazing sharply upon the strangers who had emerged from the long spiral
            stairway</p>

    </header>
    @include('_timeline',['tweets'=>$tweets])
</x-app>