<x-app>

    <form method="POST" action="{{route('profile',$user)}}" enctype="multipart/form-data">
        @csrf
        @method('PATCH')
        <div class=mb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-700" for="name">Name
            </label>
            <input class="border border-gray-400 py-2 w-full" type="text" name="name" id="name" value="{{$user->name}}"
                required>
            @error('name')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <div class=mb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-700" for="username">Username
            </label>
            <input class="border border-gray-400 py-2 w-full" type="text" name="username" id="username"
                value="{{$user->username}}" required>
            @error('username')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <div class=mb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-700" for="avatar">Avatar
            </label>
            <div class="flex">
                <input class="border border-gray-400 py-2 w-full" type="file" name="avatar" id="avatar">
                <img src="{{$user->avatar}}" alt='your avatar' width="30px">
            </div>
            @error('avatar')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>


        <div class=mb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-700" for="email">Email
            </label>
            <input class="border border-gray-400 py-2 w-full" type="email" name="email" id="email"
                value="{{$user->email}}" required>
            @error('email')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <div class=mb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-700" for="password">Password
            </label>
            <input class="border border-gray-400 py-2 w-full" type="password" name="password" id="password">
            @error('password')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>


        <div class=mb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-700" for="password-confirm"> Confirm Password
            </label>
            <input class="border border-gray-400 py-2 w-full" type="password" name="password_confirmation"
                id="password-confirm">
            @error('password-confirm')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>
        <div class="mb-6">
            <button type="submit"
                class="rounded-full shadow bg-blue-500 text-black text-white text-xs py-2 px-4 m-4">Update user</button>
                <a href="{{route('profile',$user)}}" class=" font-bold hover:underline ">Cancel</a>
        </div>
    </form>
</x-app>