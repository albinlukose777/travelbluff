<x-master>
    @section('content')
    <div class="container mx-auto flex justify-center">
        <div class="px-16 py-8 rounded-lg bg-gray-200">
            <div class="font-bold text-lg mb-4">{{ __('Register') }}</div>
            <form method="POST" action="{{ route('register') }}">
                @csrf
    
                <div class="mb-6">
    
                    <label for="name" class="block mb-2 uppercase font-bold text-sm text-gray-700">Name
                    </label>
                    <input id="name" type="text" class="border border-gray-400 p-2 w-full rounded" name="name"
                        value="{{ old('name') }}" required autocomplete="name" autofocus>
                    @error('name')
                    <p class="text-red-500 txet-xs mt-2">{{$message}}</p>
                    @enderror
    
                </div>
                <div class="mb-6">
    
                    <label for="username" class="block mb-2 uppercase font-bold text-sm text-gray-700">User Name
                    </label>
                    <input id="username" type="text" class="border border-gray-400 p-2 w-full rounded" name="username"
                        value="{{ old('username') }}" required autocomplete="username" autofocus>
                    @error('username')
                    <p class="text-red-500 txet-xs mt-2">{{$message}}</p>
                    @enderror
    
                </div>
    
                <div class="mb-6">
    
                    <label for="email" class="block mb-2 uppercase font-bold text-sm text-gray-700">Email
                    </label>
                    <input id="email" type="email" class="border border-gray-400 p-2 w-full rounded" name="email"
                        value="{{ old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                    <p class="text-red-500 txet-xs mt-2">{{$message}}</p>
                    @enderror
    
                </div>
                
                <div class="mb-6">
    
                    <label for="password" class="block mb-2 uppercase font-bold text-sm text-gray-700">password
                    </label>
                    <input id="password" type="password" class="border border-gray-400 p-2 w-full rounded" name="password"
                         required autocomplete="current-password" autofocus>
                    @error('password')
                    <p class="text-red-500 txet-xs mt-2">{{$message}}</p>
                    @enderror
    
                </div>
    
                <div class="mb-6">
    
                    <label for="password-confirm" class="block mb-2 uppercase font-bold text-sm text-gray-700">Confirm password
                    </label>
                    <input id="password-confirm" type="password" class="border border-gray-400 p-2 w-full rounded" name="password_confirmation"
                         required autocomplete="new-password" autofocus>
                    @error('password-confirm')
                    <p class="text-red-500 txet-xs mt-2">{{$message}}</p>
                    @enderror
                    
    
                </div>
    
                <div class="mb-6">
                    <button 
                    type="submit"
                    class="bg-blue-400 text-white rounded py-2 px-4 hover:bg-blue-500 mr-2">Register</button>
                </div>
    
    
                
    
            </form>
        </div>
        
    </div>
</x-master>