<x-app>
<div>
    @foreach ($users as $user)
    <a href="{{route('profile',$user)}}" class="flex items-center mb-2">
<img src="{{$user->avatar}}"
    alt="avatar"
    width=50
    class="rounded-full mr-4"
    >
    <h4 class="font-bold">{{$user->name}}</h4>
    </a>
    @endforeach
    {{$users->links()}}
</div>   
</x-app>